import React from 'react';
import Card from 'react-bootstrap/Card';
import image from '../images/logo512.png';

const Project = ({ eachProject }) => {
	return (
		<Card className="projects">
			<Card.Img variant="top" src={image} />
			<Card.Body>
				<Card.Title>{eachProject.name} </Card.Title>
				<Card.Text>{eachProject.description} </Card.Text>
			</Card.Body>
		</Card>
	);
};

export default Project;
