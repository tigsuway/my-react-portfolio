import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const ContactForm = () => {
	return (
		<Form>
			<Form.Group controlId="fullname">
				<Form.Label>Full Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your fullname."
				/>
			</Form.Group>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please enter your email."
				/>
				<small>We'll never share your email with anyone else.</small>
			</Form.Group>
			<Form.Group controlId="message">
				<Form.Label>Please enter you message.</Form.Label>
				<Form.Control as="textarea" rows={3} />
			</Form.Group>
			<Button variant="warning">Send</Button>{' '}
		</Form>
	);
};

export default ContactForm;
