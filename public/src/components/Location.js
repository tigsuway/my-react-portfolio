import React from 'react';

const Map = () => {
	return (
		<iframe
			src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3925.5926818573407!2d123.90128151476725!3d10.294365792649737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a99be19dbb042f%3A0x3bf26b1658834a85!2sWDC%20Building%2C%20Cebu%20City%2C%206000%20Cebu!5e0!3m2!1sen!2sph!4v1608035898009!5m2!1sen!2sph"
			width="600"
			height="450"
			frameBorder="0"
			style={{ border: 0 }}
			allowFullScreen=""
			aria-hidden="false"
			tabIndex="0"
			title="my-map"
		></iframe>
	);
};

export default Map;
