import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import '../App.css';
// import avatar from '../images/avatar.jpg';
import profilepic from '../images/profile-pic.jpg';
import twitter from '../images/twitter.png';
import facebook from '../images/facebook.png';
import linkedin from '../images/linkedin.png';
import instagram from '../images/instagram.png';

const Home = () => {
	return (
		// <Container className="content">
		<Container className="container" fluid="md">
			<Row>
				<Col></Col>
				<Col md={8}>
					<Row id="head">
						<Col>
							<h1 className="head">MELLOMAR.XYZ</h1>
						</Col>
					</Row>
					<Row>
						<Col>
							<p className="bio">
								Hello! My name is Mellomar Otarra, and I am a
								Cebu-based (Philippines) freelance Fullstack
								Developer and QA Automation Engineer. If I am
								not developing cool and useful web apps, I am
								doing manual testing or doing some test
								automation scripts for my clients all over the
								world.
							</p>
							<p className="bio">
								I have a diverse set of skills ranging from web
								development{' '}
								<strong>
									HTML, CSS, Javascript, React, Node, Express,
									MongoDB
								</strong>{' '}
								to software test automation{' '}
								<strong>
									Robot Framework, Cypress, Selenium, Appium,
									Gherkin,
								</strong>{' '}
								as well as{' '}
								<strong>
									Linux Administration, and Agile Scrum and
									Kanban.
								</strong>
							</p>
						</Col>
					</Row>
				</Col>
				<Col></Col>
			</Row>
			<Row>
				<Col></Col>
				<Col lg={6}>
					<Row>
						<Col>
							<img className="avatar" src={profilepic} alt="Me" />
						</Col>
					</Row>
					<Row>
						<Col></Col>
						<Col xs={7}>
							<Row>
								<Col className="socials">
									<img
										className="social"
										src={linkedin}
										alt="avatar"
									/>
								</Col>
								<Col className="socials">
									<img
										className="social"
										src={facebook}
										alt="avatar"
									/>
								</Col>
								<Col className="socials">
									<img
										className="social"
										src={instagram}
										alt="avatar"
									/>
								</Col>
								<Col className="socials">
									<img
										className="social"
										src={twitter}
										alt="avatar"
									/>
								</Col>
							</Row>
						</Col>
						<Col></Col>
					</Row>
				</Col>
				<Col></Col>
			</Row>
		</Container>
	);
};

export default Home;
