import React from 'react';

const Footer = () => {
	return (
		<div className="footer">
			<p>
				<em>Copyright © 2021</em> Mellomar Otarra.
			</p>
		</div>
	);
};

export default Footer;
