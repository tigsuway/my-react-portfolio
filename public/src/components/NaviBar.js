import React from 'react';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';

const NaviBar = () => {
	return (
		<Nav
			variant="pills"
			className="justify-content-center"
			defaultActiveKey="#home"
		>
			<Nav.Item>
				<Nav.Link as={Link} eventKey="#home" to="/home">
					HOME
				</Nav.Link>
			</Nav.Item>
			<Nav.Item>
				<Nav.Link as={Link} eventKey="#projects" to="/projects">
					PROJECTS
				</Nav.Link>
			</Nav.Item>
			<Nav.Item>
				<Nav.Link as={Link} eventKey="#skills" to="/skills">
					SKILLS
				</Nav.Link>
			</Nav.Item>
			<Nav.Item>
				<Nav.Link as={Link} eventKey="#contacts" to="/contacts">
					CONTACT
				</Nav.Link>
			</Nav.Item>
		</Nav>
	);
};

export default NaviBar;
