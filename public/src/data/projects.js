const projectData = [
	{
		id: 'p001',
		name: 'My Portfolio Using Bootstrap',
		description: 'This project was created using HTML, CSS and Bootstrap.',
		image: 'project-1.png',
		link: 'http://google.com',
		isActive: true,
	},
	{
		id: 'p002',
		name: 'Online Course Booking Using MEN Stack',
		description:
			'This project was created using HTML, CSS and Bootstrap, Express, Node and MongoDB.',
		image: 'project-2.png',
		link: 'http://google.com',
		isActive: true,
	},
	{
		id: 'p003',
		name: 'My Portfolio Revision Using React JS',
		description:
			'This project was created using HTML, CSS, React, React Bootstrap and Node.',
		image: 'project-2.png',
		link: 'http://google.com',
		isActive: true,
	},
	{
		id: 'p004',
		name: 'Online Gym Reservation',
		description: 'This project was created using HTML, CSS, and MERN Stack',
		image: 'project-2.png',
		link: 'http://google.com',
		isActive: true,
	},
];

export default projectData;
