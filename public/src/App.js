import React from 'react';
import NaviBar from './components/NaviBar';
import Projects from './pages/Projects';
import Home from './pages/Home';
import Skills from './pages/Skills';
import Contact from './pages/Contact';
import Footer from './components/Footer';

import './App.css';
import Container from 'react-bootstrap/Container';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

function App() {
	return (
		<React.Fragment>
			<BrowserRouter>
				<NaviBar />
				<Container className="parent-container">
					<Switch>
						<Route exact path="/home" component={Home} />
						<Route exact path="/projects" component={Projects} />
						<Route exact path="/skills" component={Skills} />
						<Contact />
					</Switch>
				</Container>
				<Footer />
			</BrowserRouter>
		</React.Fragment>
	);
}

export default App;
