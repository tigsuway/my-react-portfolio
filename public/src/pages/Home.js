import React from 'react';
import AboutMe from '../components/AboutMe';

const Home = () => {
	return (
		<React.Fragment>
			<AboutMe />
		</React.Fragment>
	);
};

export default Home;
