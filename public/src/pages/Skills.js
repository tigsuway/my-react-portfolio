import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import html from '../images/html5.png';
import css from '../images/css.png';
import javascript from '../images/javascript.png';
import mongodb from '../images/mongodb.png';
import expressjs from '../images/expressjs.png';
import reactjs from '../images/reactjs.png';
import nodejs from '../images/nodejs.png';
import git from '../images/git.png';
import robotframework from '../images/robotframework.png';
import cypress from '../images/cypress.png';
import selenium from '../images/selenium.png';
import appium from '../images/appium.png';
import browserstack from '../images/browserstack.png';
import saucelabs from '../images/saucelabs.png';
import postman from '../images/postman.png';
import '../App.css';

const Skills = () => {
	return (
		<Container className="skill-container">
			<Row>
				<Col md={12}>
					<h1 className="head">Development Tech Stack</h1>
				</Col>
				<Col>
					<img className="skills" src={html} alt="HTML" />
					<p>HTML</p>
				</Col>
				<Col>
					<img className="skills" src={css} alt="CSS" />
					<p>CSS</p>
				</Col>
				<Col>
					<img className="skills" src={javascript} alt="Javascript" />
					<p>JAVASCRIPT</p>
				</Col>
				<Col>
					<img className="skills" src={mongodb} alt="MongoDB" />
					<p>MONGODB</p>
				</Col>
				<Col>
					<img className="skills" src={expressjs} alt="ExpressJS" />
					<p>EXPRESSJS</p>
				</Col>
				<Col>
					<img className="skills" src={reactjs} alt="Reactjs" />
					<p>REACTJS</p>
				</Col>
				<Col>
					<img className="skills" src={nodejs} alt="Nodejs" />
					<p>NODEJS</p>
				</Col>
				<Col>
					<img className="skills" src={git} alt="GIT" />
					<p>GIT</p>
				</Col>
			</Row>
			<hr />
			<Row>
				<Col md={12}>
					<h1 className="head">Testing Frameworks And Tools</h1>
				</Col>
				<Col>
					<img
						className="skills"
						src={robotframework}
						alt="Robotframework"
					/>
					<p>ROBOTFRAMEWORK</p>
				</Col>
				<Col>
					<img className="skills" src={cypress} alt="Cypress" />
					<p>CYPRESS</p>
				</Col>
				<Col>
					<img className="skills" src={selenium} alt="Selenium" />
					<p>SELENIUM</p>
				</Col>
				<Col>
					<img className="skills" src={appium} alt="Appium" />
					<p>APPIUM</p>
				</Col>
				<Col>
					<img
						className="skills"
						src={browserstack}
						alt="Browserstack"
					/>
					<p>BROWSERSTACK</p>
				</Col>
				<Col>
					<img className="skills" src={saucelabs} alt="Saucelabs" />
					<p>SAUCELABS</p>
				</Col>
				<Col>
					<img className="skills" src={postman} alt="Postman" />
					<p>POSTMAN</p>
				</Col>
			</Row>
		</Container>
	);
};

export default Skills;
