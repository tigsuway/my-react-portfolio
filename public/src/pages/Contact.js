import React from 'react';
import Map from '../components/Location';
import { Container, Row, Col } from 'react-bootstrap';
import ContactForm from '../components/ContactForm';

const Contact = () => {
	return (
		<React.Fragment>
			<Container className="form-container">
				<h1 className="head">Contact Me</h1>
				<Row>
					<Col md={12}>
						<ContactForm />
					</Col>
				</Row>
			</Container>
			<hr />
			<Container className="map-container">
				<h1 className="head">My Location</h1>
				<Row>
					<Col className="col-md-12">
						<Row className="row">
							<Col className="col-md-12">
								<address>
									Mezzanine 2A WDC-Building P. Burgos St.
									corner Osmena Blvd. Cebu City 6000,
									Philippines
								</address>
							</Col>
						</Row>
					</Col>
				</Row>
				<Row>
					<Col md={12} className="map-responsive">
						<Map />
					</Col>
				</Row>
			</Container>
		</React.Fragment>
	);
};

export default Contact;
