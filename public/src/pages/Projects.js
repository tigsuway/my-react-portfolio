import React from 'react';
import Project from '../components/Project';
import projectsData from '../data/projects';
import { Row, Col, Container, CardDeck } from 'react-bootstrap';

const Projects = () => {
	const projects = projectsData.map((project) => {
		return (
			<Col
				key={project.id}
				className="col-md-4 d-flex align-items-stretch"
			>
				<Project eachProject={project} />
			</Col>
		);
	});
	return (
		<React.Fragment>
			<Container className="card-container">
				<h1 className="head"> My Projects </h1>
				<Row>
					<CardDeck>{projects}</CardDeck>
				</Row>
			</Container>
		</React.Fragment>
	);
};

export default Projects;
